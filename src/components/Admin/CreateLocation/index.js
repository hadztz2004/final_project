import { message } from 'antd';
import { useState } from 'react';
import { useSelector } from 'react-redux';
import { createLocation } from '~/services/admin';
import { Button, Form, Modal } from 'react-bootstrap';

function CreateLocation({ show, setShow }) {
    const [location, setLocation] = useState('');
    const [country, setCountry] = useState('');
    const [province, setProvince] = useState('');

    const currentUser = useSelector((state) => state.auth.currentUser);

    const handleCreateLocation = async () => {
        if (location && country && province) {
            const newLocation = {
                tenViTri: location,
                tinhThanh: province,
                quocGia: country,
            };
            const result = await createLocation(newLocation, currentUser.token);
            setShow(false);
            message.success(result.message);

            setTimeout(() => {
                window.location.reload();
            }, 1000);
        } else {
            alert('Vui lòng nhập đầy đủ thông tin');
        }
    };

    return (
        <Modal show={show} onHide={() => setShow(false)}>
            <Modal.Header closeButton>
                <Modal.Title>Thêm mới vị trí</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <Form.Group className="mb-3">
                    <Form.Label>Vị trí</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Nhập tên vị trí"
                        value={location}
                        onChange={(e) => setLocation(e.target.value)}
                    />
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Quốc gia</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Nhập tên quốc gia"
                        value={country}
                        onChange={(e) => setCountry(e.target.value)}
                    />
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Tỉnh thành</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Nhập tên tỉnh thành"
                        value={province}
                        onChange={(e) => setProvince(e.target.value)}
                    />
                </Form.Group>
            </Modal.Body>
            <Modal.Footer>
                <Button size="sm" variant="secondary">
                    Đóng
                </Button>
                <Button size="sm" variant="primary" onClick={handleCreateLocation}>
                    Thêm mới
                </Button>
            </Modal.Footer>
        </Modal>
    );
}

export default CreateLocation;
