import { useState } from 'react';
import { Link } from 'react-router-dom';
import logo from '~/assets/image/long-logo.png';
import Container from 'react-bootstrap/Container';
import { Navbar, Card, Offcanvas } from 'react-bootstrap';

const menus = [
    {
        icon: 'users',
        path: '/admin',
        title: 'Quản lý người dùng',
    },
    {
        icon: 'location-dot',
        path: '/admin/location',
        title: 'Quản lý thông tin vị trí',
    },
    {
        icon: 'hospital',
        path: '/admin/rooms',
        title: 'Quản lý thông tin phòng',
    },
    {
        icon: 'bookmark',
        path: '/admin/book-room',
        title: 'Quản lý đặt phòng',
    },
];

function Header() {
    const [show, setShow] = useState(false);
    document.title = 'Quản trị website';

    return (
        <Navbar className="border-b">
            <Container>
                <Navbar.Brand>
                    <button onClick={() => setShow(true)}>
                        <i className="fa-solid fa-bars"></i>
                    </button>
                    <Offcanvas show={show} onHide={() => setShow(false)}>
                        <Offcanvas.Header closeButton>
                            <Offcanvas.Title className="mx-auto">
                                <Card.Img src={logo} alt="" style={{ width: 100 }} />
                            </Offcanvas.Title>
                        </Offcanvas.Header>
                        <Offcanvas.Body>
                            <ul>
                                {menus.map((menu, index) => (
                                    <li className="mb-3" key={index} onClick={() => setShow(false)}>
                                        <Link to={menu.path}>
                                            <i className={`fa-solid fa-${menu.icon}`}></i>
                                            <span className="ml-2">{menu.title}</span>
                                        </Link>
                                    </li>
                                ))}
                            </ul>
                        </Offcanvas.Body>
                    </Offcanvas>
                </Navbar.Brand>
            </Container>
        </Navbar>
    );
}
export default Header;
