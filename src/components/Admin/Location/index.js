import { message } from 'antd';
import Wrapper from '../Wrapper';
import { useSelector } from 'react-redux';
import CreateLocation from '../CreateLocation';
import noImage from '~/assets/image/no-image.jpg';
import { useEffect, useRef, useState } from 'react';
import { Button, Card, Table } from 'react-bootstrap';
import { deleteLocation, getLocation, uploadImageLocation } from '~/services/admin';

function Location() {
    const [show, setShow] = useState(false);
    const [location, setLocation] = useState([]);
    const [locationId, setLocationId] = useState(0);

    const imageRef = useRef();
    const currentUser = useSelector((state) => state.auth.currentUser);

    useEffect(() => {
        const fetchApi = async () => {
            const result = await getLocation();
            setLocation(result.content);
        };
        fetchApi();
    }, []);

    const handleDelete = async (id) => {
        const result = await deleteLocation(id, currentUser.token);

        if (result.statusCode === 200) {
            message.success(result.message);

            setTimeout(() => {
                window.location.reload();
            }, 1000);
        } else {
            message.error(result.content);
        }
    };

    const handleSelectImage = async (e) => {
        let file = e.target.files[0];

        let formData = new FormData();
        formData.append('formFile', file);

        const result = await uploadImageLocation(locationId, formData, currentUser.token);

        if (result.statusCode === 200) {
            message.success('Thêm ảnh thành công');

            setTimeout(() => {
                window.location.reload();
            }, 1000);
        } else {
            message.error(result.message);
        }
    };

    const handleClick = (id) => {
        setLocationId(id);
        imageRef.current.click();
    };

    return (
        <Wrapper>
            <Card>
                <Card.Header>
                    <Card.Title className="inline-block">Quản lí vị trí</Card.Title>

                    <div className="float-end">
                        <Button size="sm" onClick={() => setShow(true)}>
                            Thêm vị trí
                        </Button>
                    </div>

                    {show && <CreateLocation show={show} setShow={setShow} />}
                </Card.Header>
                <Card.Body>
                    <Table striped bordered>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Quốc gia</th>
                                <th>Tỉnh thành</th>
                                <th>Vị trí</th>
                                <th>Ảnh</th>
                                <th>Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                            {location.map((location) => (
                                <tr key={location.id}>
                                    <td>{location.id}</td>
                                    <td>{location.quocGia}</td>
                                    <td>{location.tinhThanh}</td>
                                    <td>{location.tenViTri}</td>
                                    <td>
                                        <img
                                            style={{ maxWidth: 160, margin: '0 auto' }}
                                            src={location.hinhAnh || noImage}
                                            alt={location.quocGia}
                                        />

                                        <Button
                                            size="sm"
                                            variant="success"
                                            className="mt-2"
                                            onClick={() => handleClick(location.id)}
                                        >
                                            Chọn ảnh
                                        </Button>

                                        <input
                                            type="file"
                                            ref={imageRef}
                                            onChange={(e) => handleSelectImage(e)}
                                            hidden
                                        />
                                    </td>
                                    <td>
                                        <Button
                                            size="sm"
                                            variant="danger"
                                            className="ml-2"
                                            onClick={() => handleDelete(location.id)}
                                        >
                                            Xóa
                                        </Button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </Card.Body>
            </Card>
        </Wrapper>
    );
}

export default Location;
