import Header from '../Header';
import React, { Fragment } from 'react';

function Wrapper({ children }) {
    return (
        <Fragment>
            <Header />
            <div className="container py-5">{children}</div>
        </Fragment>
    );
}

export default Wrapper;
