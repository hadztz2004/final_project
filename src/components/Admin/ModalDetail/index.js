import { useState } from 'react';
import { Button, Col, Form, Modal, Table } from 'react-bootstrap';

function ModalDetail({ show, setShow, data }) {
    const [name, setName] = useState(data?.tenPhong);
    const [price, setPrice] = useState(data?.giaTien);
    const [bed, setBed] = useState(data?.giuong);
    const [guests, setGuests] = useState(data?.khach);
    const [parking, setParking] = useState(data?.doXe);
    const [bedroom, setBedroom] = useState(data?.phongNgu);
    const [bathroom, setBathroom] = useState(data?.phongTam);
    const [irons, setIrons] = useState(data?.banLa);
    const [ironingBoard, setIroningBoard] = useState(data?.banUi);
    const [kitchen, setKitchen] = useState(data?.bep);
    const [airConditional, setAirConditional] = useState(data?.dieuHoa);
    const [pool, setPool] = useState(data?.hoBoi);
    const [location, setLocation] = useState(data?.maViTri);
    const [washing, setWashing] = useState(data?.mayGiat);
    const [tivi, setTivi] = useState(data?.tivi);
    const [wifi, setWifi] = useState(data?.wifi);

    return (
        <Modal size="xl" show={show} onHide={() => setShow(false)}>
            <Modal.Header closeButton>
                <Modal.Title>{name}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Table striped bordered>
                    <thead>
                        <tr>
                            <th>Tên</th>
                            <th>Giá trị</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>Tên phòng</td>
                            <td>
                                <Col sm={10}>
                                    <Form.Control
                                        type="text"
                                        placeholder="Nhập tên phòng"
                                        value={name}
                                        onChange={(e) => setName(e.target.value)}
                                    />
                                </Col>
                            </td>
                        </tr>
                        <tr>
                            <td>Giá phòng</td>
                            <td>
                                <Col sm={10}>
                                    <Form.Control
                                        type="text"
                                        placeholder="Nhập giá tiền thuê phòng"
                                        value={price}
                                        onChange={(e) => setPrice(e.target.value)}
                                    />
                                </Col>
                            </td>
                        </tr>
                        <tr>
                            <td>Số giường</td>
                            <td>
                                <Col sm={10}>
                                    <Form.Control
                                        type="text"
                                        placeholder="Nhập số giường của phòng"
                                        value={bed}
                                        onChange={(e) => setBed(e.target.value)}
                                    />
                                </Col>
                            </td>
                        </tr>
                        <tr>
                            <td>Số khách đang thuê</td>
                            <td>
                                <Col sm={10}>
                                    <Form.Control
                                        type="text"
                                        placeholder="Nhập số khách đang thuê"
                                        value={guests}
                                        onChange={(e) => setGuests(e.target.value)}
                                    />
                                </Col>
                            </td>
                        </tr>
                        <tr>
                            <td>Số phòng ngủ</td>
                            <td>
                                <Col sm={10}>
                                    <Form.Control
                                        type="text"
                                        placeholder="Nhập số phòng ngủ"
                                        value={bedroom}
                                        onChange={(e) => setBedroom(e.target.value)}
                                    />
                                </Col>
                            </td>
                        </tr>
                        <tr>
                            <td>Số phòng tắm</td>
                            <td>
                                <Col sm={10}>
                                    <Form.Control
                                        type="text"
                                        placeholder="Nhập số phòng tắm"
                                        value={bathroom}
                                        onChange={(e) => setBathroom(e.target.value)}
                                    />
                                </Col>
                            </td>
                        </tr>
                        <tr>
                            <td>Mã vị trí</td>
                            <td>
                                <Col sm={10}>
                                    <Form.Control
                                        type="text"
                                        placeholder="Nhập số phòng tắm"
                                        value={location}
                                        onChange={(e) => setLocation(e.target.value)}
                                    />
                                </Col>
                            </td>
                        </tr>
                        <tr>
                            <td>Chỗ đỗ xe</td>
                            <td>
                                <Col sm={10}>
                                    <Form.Select value={parking} onChange={(e) => setParking(e.target.value)}>
                                        <option value={true}>Có</option>
                                        <option value={false}>Không</option>
                                    </Form.Select>
                                </Col>
                            </td>
                        </tr>
                        <tr>
                            <td>Bàn là</td>
                            <td>
                                <Col sm={10}>
                                    <Form.Select value={irons} onChange={(e) => setIrons(e.target.value)}>
                                        <option value={true}>Có</option>
                                        <option value={false}>Không</option>
                                    </Form.Select>
                                </Col>
                            </td>
                        </tr>
                        <tr>
                            <td>Bàn ủi</td>
                            <td>
                                <Col sm={10}>
                                    <Form.Select value={ironingBoard} onChange={(e) => setIroningBoard(e.target.value)}>
                                        <option value={true}>Có</option>
                                        <option value={false}>Không</option>
                                    </Form.Select>
                                </Col>
                            </td>
                        </tr>
                        <tr>
                            <td>Bếp</td>
                            <td>
                                <Col sm={10}>
                                    <Form.Select value={kitchen} onChange={(e) => setKitchen(e.target.value)}>
                                        <option value={true}>Có</option>
                                        <option value={false}>Không</option>
                                    </Form.Select>
                                </Col>
                            </td>
                        </tr>
                        <tr>
                            <td>Điều hòa</td>
                            <td>
                                <Col sm={10}>
                                    <Form.Select
                                        value={airConditional}
                                        onChange={(e) => setAirConditional(e.target.value)}
                                    >
                                        <option value={true}>Có</option>
                                        <option value={false}>Không</option>
                                    </Form.Select>
                                </Col>
                            </td>
                        </tr>
                        <tr>
                            <td>Hồ bơi</td>
                            <td>
                                <Col sm={10}>
                                    <Form.Select value={pool} onChange={(e) => setPool(e.target.value)}>
                                        <option value={true}>Có</option>
                                        <option value={false}>Không</option>
                                    </Form.Select>
                                </Col>
                            </td>
                        </tr>
                        <tr>
                            <td>Máy giặt</td>
                            <td>
                                <Col sm={10}>
                                    <Form.Select value={washing} onChange={(e) => setWashing(e.target.value)}>
                                        <option value={true}>Có</option>
                                        <option value={false}>Không</option>
                                    </Form.Select>
                                </Col>
                            </td>
                        </tr>
                        <tr>
                            <td>Tivi</td>
                            <td>
                                <Col sm={10}>
                                    <Form.Select value={tivi} onChange={(e) => setTivi(e.target.value)}>
                                        <option value={true}>Có</option>
                                        <option value={false}>Không</option>
                                    </Form.Select>
                                </Col>
                            </td>
                        </tr>
                        <tr>
                            <td>Wifi</td>
                            <td>
                                <Col sm={10}>
                                    <Form.Select value={wifi} onChange={(e) => setWifi(e.target.value)}>
                                        <option value={true}>Có</option>
                                        <option value={false}>Không</option>
                                    </Form.Select>
                                </Col>
                            </td>
                        </tr>
                    </tbody>
                </Table>
            </Modal.Body>
            <Modal.Footer>
                <Button size="sm" variant="secondary" onClick={() => setShow(false)}>
                    Đóng
                </Button>
            </Modal.Footer>
        </Modal>
    );
}

export default ModalDetail;
