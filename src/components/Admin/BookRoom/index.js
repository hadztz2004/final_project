import { message } from 'antd';
import Wrapper from '../Wrapper';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { useEffect, useState } from 'react';
import { Button, Card, Table } from 'react-bootstrap';
import { cancelBooking, getBookRoom } from '~/services/admin';

function BookRoom() {
    const [bookRoom, setBookRoom] = useState([]);

    const currentUser = useSelector((state) => state.auth.currentUser);

    useEffect(() => {
        const fetchApi = async () => {
            const result = await getBookRoom();

            setBookRoom(result.content);
        };
        fetchApi();
    }, []);

    const handleCancelBooking = async (id) => {
        const result = await cancelBooking(currentUser.token, id);

        if (result.statusCode !== 200) {
            message.error('Hủy lịch đặt thất bại');
        } else {
            message.success('Hủy lịch đặt thành công');

            setTimeout(() => {
                window.location.reload();
            }, 1000);
        }
    };

    return (
        <Wrapper>
            <Card>
                <Card.Header>
                    <Card.Title>Quản lý đặt phòng</Card.Title>
                </Card.Header>

                <Card.Body>
                    <Table striped bordered>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Người đặt</th>
                                <th>Số lượng khách</th>
                                <th>Mã phòng</th>
                                <th>Ngày đến / đi</th>
                                <th>Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                            {bookRoom.map((room) => (
                                <tr key={room.id}>
                                    <td>{room.id}</td>
                                    <td>
                                        <Link to={`/admin/users/${room.maNguoiDung}`}>{room.maNguoiDung}</Link>
                                    </td>
                                    <td>{room.soLuongKhach}</td>
                                    <td>{room.maPhong}</td>
                                    <td>
                                        <div>{room.ngayDen}</div>
                                        <div>{room.ngayDi}</div>
                                    </td>
                                    <td>
                                        <Button variant="danger" size="sm" onClick={() => handleCancelBooking(room.id)}>
                                            Hủy lịch
                                        </Button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </Card.Body>
            </Card>
        </Wrapper>
    );
}

export default BookRoom;
