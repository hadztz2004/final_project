import moment from 'moment';
import { message } from 'antd';
import { useEffect, useState } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { createUser, updateUser } from '~/services/user';
import { closeModalForm } from '~/redux/reducer/modalReducer';

function ModalForm() {
    const [email, setEmail] = useState('');
    const [gender, setGender] = useState('');
    const [name, setName] = useState('');
    const [phone, setPhone] = useState('');
    const [role, setRole] = useState('');
    const [birthday, setBirthday] = useState('');
    const [password, setPassword] = useState('');

    const dispatch = useDispatch();
    const { status, type, data } = useSelector((state) => state.modal.modalForm);

    useEffect(() => {
        if (data) {
            const date = moment(data.birthday, 'DD/MM/YYYY').format('YYYY-MM-DD');

            setEmail(data.email);
            setGender(data.gender);
            setName(data.name);
            setPhone(data.phone ? data.phone : '');
            setRole(data.role);
            setBirthday(date);
        }
    }, [data]);

    const handleSavaChanges = async () => {
        if (type === 'create-user') {
            if (email && gender && name && phone && role && birthday && password) {
                const newUser = {
                    name,
                    email,
                    password,
                    phone,
                    birthday,
                    gender,
                    role,
                };

                const result = await createUser(newUser);

                if (result.statusCode === 200) {
                    dispatch(closeModalForm());
                    message.success('Tạo người dùng thành công');
                    setTimeout(() => {
                        window.location.reload();
                    }, 1000);
                }
            } else {
                alert('Vui lòng nhập đủ thông tin');
            }
        } else {
            const regexEmail = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
            if (!regexEmail.test(email) || !email) {
                alert('Email không đúng định dạng');
                return;
            }
            if (!name) {
                alert('Họ tên là bắt buộc');
                return;
            }

            const userUpdate = {
                name,
                email,
                phone,
                birthday,
                gender,
                role,
            };

            const result = await updateUser(userUpdate, data.id);

            if (result.statusCode === 200) {
                dispatch(closeModalForm());
                message.success('Cập nhật thành công');
                setTimeout(() => {
                    window.location.reload();
                }, 1000);
            } else {
                alert(result.content);
            }
        }
    };

    let formTitle;

    switch (type) {
        case 'create-user':
            formTitle = 'Thêm người dùng mới';
            break;
        case 'edit-user':
            formTitle = 'Sửa thông tin người dùng';
            break;
        default:
            formTitle = '';
    }

    return (
        <Modal show={status} onHide={() => dispatch(closeModalForm())}>
            <Modal.Header closeButton>
                <Modal.Title>{formTitle}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form.Group className="mb-3">
                    <Form.Label>Email</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter your email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                    />
                </Form.Group>

                {type === 'create-user' && (
                    <Form.Group className="mb-3">
                        <Form.Label>Mật khẩu</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter your password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                    </Form.Group>
                )}

                <Form.Group className="mb-3">
                    <Form.Label>Giới tính</Form.Label>
                    <select className="form-control" value={gender} onChange={(e) => setGender(e.target.value)}>
                        <option value={true}>Male</option>
                        <option value={false}>Female</option>
                    </select>
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Họ tên</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter your name"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                    />
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>SĐT</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter your phone"
                        value={phone}
                        onChange={(e) => setPhone(e.target.value)}
                    />
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Vai trò</Form.Label>
                    <Form.Select value={role} onChange={(e) => setRole(e.target.value)}>
                        <option value="ADMIN">ADMIN</option>
                        <option value="USER">USER</option>
                    </Form.Select>
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Ngày sinh</Form.Label>
                    <Form.Control type="date" value={birthday} onChange={(e) => setBirthday(e.target.value)} />
                </Form.Group>
            </Modal.Body>
            <Modal.Footer>
                <Button size="sm" variant="secondary" onClick={() => dispatch(closeModalForm())}>
                    Đóng
                </Button>
                <Button size="sm" variant="primary" onClick={handleSavaChanges}>
                    {type === 'create-user' ? 'Thêm mới' : 'Cập nhật'}
                </Button>
            </Modal.Footer>
        </Modal>
    );
}

export default ModalForm;
