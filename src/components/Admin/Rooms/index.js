import { message } from 'antd';
import Wrapper from '../Wrapper';
import ModalDetail from '../ModalDetail';
import { useSelector } from 'react-redux';
import { useEffect, useState } from 'react';
import noImage from '~/assets/image/no-image.jpg';
import { Button, Card, Table } from 'react-bootstrap';
import { deleteRoom, getRooms } from '~/services/admin';

function Rooms() {
    const [rooms, setRooms] = useState([]);
    const [show, setShow] = useState(false);
    const [roomDetail, setRoomDetail] = useState(null);

    const currentUser = useSelector((state) => state.auth.currentUser);

    useEffect(() => {
        const fetchApi = async () => {
            const result = await getRooms();
            setRooms(result.content);
        };
        fetchApi();
    }, []);

    const handleDelete = async (id) => {
        const result = await deleteRoom(currentUser.token, id);

        if (result.statusCode === 200) {
            message.success(result.message);

            setTimeout(() => {
                window.location.reload();
            }, 1000);
        } else {
            message.error('Xóa thất bại');
        }
    };

    const handleOpenModal = (room) => {
        setRoomDetail(room);
        setShow(true);
    };

    return (
        <Wrapper>
            <Card>
                <Card.Footer>
                    <Card.Title>Quản lý thông tin phòng</Card.Title>
                </Card.Footer>

                <Card.Body>
                    <Table striped bordered>
                        <thead>
                            <tr>
                                <th>Tên</th>
                                <th>Giá</th>
                                <th>Giường</th>
                                <th>Khách</th>
                                <th>Đỗ xe</th>
                                <th>Ngủ / tắm</th>
                                <th>Ảnh</th>
                                <th>Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                            {rooms.map((room) => (
                                <tr key={room.id}>
                                    <td>{room.tenPhong}</td>
                                    <td>{room.giaTien}$</td>
                                    <td>{room.giuong}</td>
                                    <td>{room.khach}</td>
                                    <td>{room.doXe ? 'Có' : 'Không'}</td>
                                    <td>
                                        {room.phongNgu} / {room.phongTam}
                                    </td>
                                    <td>
                                        <img
                                            style={{ maxWidth: 200, margin: '0 auto' }}
                                            src={room.hinhAnh || noImage}
                                            alt={room.tenPhong}
                                        />
                                    </td>
                                    <td>
                                        <Button size="sm" onClick={() => handleOpenModal(room)}>
                                            Chi tiết
                                        </Button>
                                        <Button
                                            size="sm"
                                            variant="danger"
                                            className="ml-2"
                                            onClick={() => handleDelete(room.id)}
                                        >
                                            Xóa
                                        </Button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>

                    {show && <ModalDetail show={show} setShow={setShow} data={roomDetail} />}
                </Card.Body>
            </Card>
        </Wrapper>
    );
}

export default Rooms;
