import { message } from 'antd';
import Wrapper from '../Wrapper';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { getUserById } from '~/services/admin';
import { Card, Col, Form } from 'react-bootstrap';

function UserDetail() {
    const [user, setUser] = useState(null);

    const { id } = useParams();

    useEffect(() => {
        if (id) {
            const fetchApi = async () => {
                const result = await getUserById(id);

                if (result.statusCode === 200) {
                    setUser(result.content);
                } else {
                    message.error(result.content);
                }
            };
            fetchApi();
        }
    }, [id]);
    return (
        <Wrapper>
            <Col sm={6}>
                <Card>
                    <Card.Header>
                        <Card.Title>Thông tin người dùng {id}</Card.Title>
                    </Card.Header>
                    <Card.Body>
                        <Form.Group className="mb-3">
                            <Form.Label>ID người dùng</Form.Label>
                            <Form.Control type="text" defaultValue={user?.id} disabled placeholder="Uid người dùng" />
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label>Email người dùng</Form.Label>
                            <Form.Control
                                type="text"
                                defaultValue={user?.email}
                                disabled
                                placeholder="Email người dùng"
                            />
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label>Tên người dùng</Form.Label>
                            <Form.Control type="text" defaultValue={user?.name} disabled placeholder="Tên người dùng" />
                        </Form.Group>
                    </Card.Body>
                </Card>
            </Col>
        </Wrapper>
    );
}

export default UserDetail;
