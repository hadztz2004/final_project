import './index.css';
import { message } from 'antd';
import Wrapper from './Wrapper';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button, Card, Table } from 'react-bootstrap';
import { deleteUser, getUsers } from '~/services/user';
import { useDispatch, useSelector } from 'react-redux';
import { openModalForm } from '~/redux/reducer/modalReducer';

function Admin() {
    const [users, setUsers] = useState([]);
    const [messageApi, contextHolder] = message.useMessage();

    const navigate = useNavigate();
    const dispatch = useDispatch();
    const currentUser = useSelector((state) => state.auth.currentUser);

    useEffect(() => {
        if (!currentUser || currentUser.role !== 'ADMIN') {
            alert('Bạn không có quyền này');
            navigate('/');
        } else {
            document.title = 'Quản trị AirBNB';
        }
    }, [currentUser, navigate]);

    useEffect(() => {
        const fetchApi = async () => {
            const result = await getUsers();

            setUsers(result.content);
        };
        fetchApi();
    }, []);

    const success = (message) => {
        messageApi.open({
            type: 'success',
            content: message,
        });
    };
    const error = (message) => {
        messageApi.open({
            type: 'error',
            content: message,
        });
    };

    const handleDelete = async (id) => {
        const result = await deleteUser(id);

        if (result.statusCode === 200) {
            const users = await getUsers();
            setUsers(users.content);
            success('Xóa thành công');
        } else {
            error('Xóa thất bại');
        }
    };

    return (
        <Wrapper>
            {contextHolder}

            <Card className="sm:overflow-hidden">
                <Card.Header>
                    <Card.Title className="inline-block">Quản lý người dùng</Card.Title>

                    <div className="float-end">
                        <Button size="sm" onClick={() => dispatch(openModalForm({ type: 'create-user', data: null }))}>
                            Thêm mới
                        </Button>
                    </div>
                </Card.Header>

                <Card.Body>
                    <Table striped bordered>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Email</th>
                                <th>Họ tên</th>
                                <th>Giới tính</th>
                                <th>Vai trò</th>
                                <th>Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                            {users.map((user) => (
                                <tr key={user.id}>
                                    <td>{user.id}</td>
                                    <td>{user.email}</td>
                                    <td>{user.name}</td>
                                    <td>{user.gender ? 'Male' : 'Female'}</td>
                                    <td>{user.role}</td>
                                    <td>
                                        <Button
                                            size="sm"
                                            onClick={() => dispatch(openModalForm({ type: 'edit-user', data: user }))}
                                        >
                                            Chi tiết
                                        </Button>
                                        <Button
                                            size="sm"
                                            variant="danger"
                                            className="ml-2"
                                            onClick={() => handleDelete(user.id)}
                                        >
                                            Xóa
                                        </Button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </Card.Body>
            </Card>
        </Wrapper>
    );
}

export default Admin;
