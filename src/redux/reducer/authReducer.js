import { createSlice } from '@reduxjs/toolkit';

const authReducer = createSlice({
    name: 'auth',
    initialState: {
        currentUser: null,
    },
    reducers: {
        loginUserSuccess: (state, action) => {
            state.currentUser = action.payload;
        },
    },
});

export const { loginUserSuccess } = authReducer.actions;

export default authReducer.reducer;
