import { createSlice } from '@reduxjs/toolkit';

const modalReducer = createSlice({
    name: 'modal',
    initialState: {
        modalForm: {
            status: false,
            type: '',
            data: null,
        },
    },
    reducers: {
        openModalForm: (state, action) => {
            state.modalForm.status = true;
            state.modalForm.type = action.payload.type;
            state.modalForm.data = action.payload.data;
        },
        closeModalForm: (state) => {
            state.modalForm.status = false;
            state.modalForm.type = '';
        },
    },
});

export const { openModalForm, closeModalForm } = modalReducer.actions;

export default modalReducer.reducer;
