import React from 'react';
import { useDispatch } from 'react-redux';
import { loginUser } from '~/services/auth';
import { Button, Form, Input, message } from 'antd';
import { NavLink, useNavigate } from 'react-router-dom';
import './login.css';

function Login() {
    let dispatch = useDispatch();
    let navigate = useNavigate();

    const onFinish = async (values) => {
        const result = await loginUser(values, dispatch);

        if (result.statusCode === 200) {
            navigate('/');
        } else {
            message.success('Đăng nhập thất bại');
        }
    };

    return (
        <div className="flex justify-center py-40 login">
            <div className="bg-white p-10 rounded shadow-sm h-3/5">
                <Form
                    name="basic"
                    labelCol={{
                        span: 8,
                    }}
                    wrapperCol={{
                        span: 24,
                    }}
                    style={{
                        maxWidth: 800,
                    }}
                    initialValues={{
                        remember: true,
                    }}
                    layout="vertical"
                    onFinish={onFinish}
                    autoComplete="off"
                >
                    <div className="flex justify-evenly mb-3">
                        <div className="text-red-500 text-2xl font-bold">
                            <i className="fab fa-airbnb text-3xl font-bold" />
                            <span className="px-1">AirBnB</span>
                        </div>
                        <h2 className="text-lime-600 text-2xl">Đăng nhập</h2>
                    </div>
                    <div>
                        <Form.Item
                            label="Email"
                            name="email"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your email!',
                                },
                            ]}
                        >
                            <Input placeholder="Email address" />
                        </Form.Item>

                        <Form.Item
                            label="Password"
                            name="password"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your password!',
                                },
                            ]}
                        >
                            <Input.Password placeholder="Password" autoComplete="on" />
                        </Form.Item>
                    </div>

                    <Form.Item
                        wrapperCol={{
                            span: 24,
                        }}
                    >
                        <div className="flex justify-start">
                            <Button className="bg-lime-600 hover:text-white rounded-2xl" htmlType="submit">
                                Đăng nhập
                            </Button>
                            <p className="px-20 leading-3 text-blue-700">Quên mật khẩu ? </p>
                        </div>
                    </Form.Item>
                    <NavLink to={'/register'} className="no-underline">
                        <span className="text-red-600 font-medium  hover:text-black ">Đăng ký ngay</span>
                    </NavLink>
                </Form>
            </div>
        </div>
    );
}

export default Login;
