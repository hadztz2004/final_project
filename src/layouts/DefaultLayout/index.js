import { Fragment } from 'react';
import Header from '~/layouts/Header';
import Footer from '~/layouts/Footer';

function DefaultLayout({ children }) {
    return (
        <Fragment>
            <Header />
            <div className="container mx-auto mt-5">{children}</div>
            <Footer />
        </Fragment>
    );
}

export default DefaultLayout;
