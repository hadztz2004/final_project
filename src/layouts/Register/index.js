import React from 'react';
import { NavLink } from 'react-router-dom';
import { registerUser } from '~/services/auth';
import { useNavigate } from 'react-router-dom';
import { Button, DatePicker, Form, Input, message, Select } from 'antd';
import './register.css';

function Register() {
    let navigate = useNavigate();

    const config = {
        rules: [
            {
                type: 'object',
                required: true,
                message: 'Please select time!',
            },
        ],
    };

    const onFinish = async (fieldsValue) => {
        let values = { ...fieldsValue, birthday: fieldsValue['birthday'].format('YYYY-MM-DD') };
        values = { ...values, id: 0, role: 'USER' };

        const result = await registerUser(values);

        if (result.statusCode === 200) {
            message.success('Đăng ký thành công');
            navigate('/login');
        } else {
            message.error('Đăng ký thất bại');
        }
    };

    return (
        <div className="register">
            <div className="rounded shadow-sm bg-white py-3.5 px-3.5 w-1/2 h-fit">
                <div className="flex justify-evenly">
                    <div className="text-red-500 text-2xl font-bold">
                        <i className="fab fa-airbnb text-3xl font-bold" />

                        <span className="px-1">AirBnB</span>
                    </div>
                    <h2 className="text-lime-600 text-2xl">Đăng ký tài khoản</h2>
                </div>
                <Form
                    name="basic"
                    labelCol={{
                        span: 8,
                    }}
                    wrapperCol={{
                        span: 24,
                    }}
                    initialValues={{
                        remember: true,
                    }}
                    layout="vertical"
                    autoComplete="off"
                    onFinish={onFinish}
                >
                    <div className="row">
                        <Form.Item
                            label="Email"
                            name="email"
                            rules={[
                                {
                                    type: 'email',
                                    message: 'The input is not valid Email!',
                                },
                                {
                                    required: true,
                                    message: 'Please input your Email!',
                                },
                            ]}
                        >
                            <Input placeholder="Email address" />
                        </Form.Item>

                        <Form.Item
                            label="Mật khẩu"
                            name="password"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your password!',
                                },
                            ]}
                        >
                            <Input.Password placeholder="Password" />
                        </Form.Item>

                        <Form.Item
                            label="Tên người dùng"
                            name="name"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your name!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="Số điện thoại"
                            name="phone"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your phone!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="Giới tính"
                            name="gender"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please choose!',
                                },
                            ]}
                        >
                            <Select>
                                <Select.Option value="true">Nam</Select.Option>
                                <Select.Option value="false">Nữ</Select.Option>
                            </Select>
                        </Form.Item>

                        <Form.Item name="birthday" label="Ngày sinh" {...config}>
                            <DatePicker />
                        </Form.Item>
                    </div>

                    <Form.Item
                        wrapperCol={{
                            span: 24,
                        }}
                    >
                        <div className="text-center w-full">
                            <Button className="bg-lime-600  rounded-3xl px-32" htmlType="submit">
                                Đăng ký
                            </Button>
                            <NavLink to={'/login'} className="no-underline">
                                <p className="text-red-600 mt-3 font-medium hover:text-black ">Đăng nhập ngay</p>
                            </NavLink>
                        </div>
                    </Form.Item>
                </Form>
            </div>
        </div>
    );
}

export default Register;
