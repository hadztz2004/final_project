import './slider.css';
import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

const sliders = [
    {
        icon: 'keybase',
        title: 'Thật ấn tượng',
    },
    {
        icon: 'swimming-pool',
        title: 'Hồ bơi tuyệt vời',
    },
    {
        icon: 'parachute-box',
        title: 'Đảo',
    },
    {
        icon: 'umbrella-beach',
        title: 'Bãi biển',
    },
    {
        icon: 'drafting-compass',
        title: 'Thiết kế',
    },
    {
        icon: 'house-damage',
        title: 'Nhà nhỏ',
    },
    {
        icon: 'snowflake',
        title: 'Bắc cực',
    },
    {
        icon: 'box',
        title: 'Carbin',
    },
    {
        icon: 'ghost',
        title: 'Nhà dưới lòng đất',
    },
    {
        icon: 'swimmer',
        title: 'Ven Hồ',
    },
    {
        icon: 'dungeon',
        title: 'Hang động',
    },
    {
        icon: 'golf-ball',
        title: 'Chơi golf',
    },
    {
        icon: 'phoenix-framework',
        title: 'Lướt sóng',
    },
    {
        icon: 'smile-beam',
        title: 'Khung cảnh tuyệt vời',
    },
];

function SliderPage() {
    let settings = {
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 6,
        slidesToScroll: 1,
    };

    return (
        <div className="sliderPage">
            <Slider {...settings}>
                {sliders.map((slider, index) => (
                    <div className="sliderFlex hover:cursor-pointer" key={index}>
                        <div className="text-center">
                            <i className={`fa fa-${slider.icon}`} />
                        </div>
                        <p className="text-center my-2 text-slate-400">{slider.title}</p>
                    </div>
                ))}
            </Slider>
        </div>
    );
}

export default SliderPage;
