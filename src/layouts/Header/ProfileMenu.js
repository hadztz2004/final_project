import './style.css';
import { Avatar } from 'antd';
import { useState } from 'react';
import Menu from '@mui/material/Menu';
import { NavLink } from 'react-router-dom';
import MenuItem from '@mui/material/MenuItem';
import { UserOutlined } from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux';
import MenuRoundedIcon from '@mui/icons-material/MenuRounded';
import { loginUserSuccess } from '~/redux/reducer/authReducer';

export default function BasicMenu() {
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);

    const dispatch = useDispatch();
    const currentUser = useSelector((state) => state.auth.currentUser);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleLogout = () => {
        dispatch(loginUserSuccess(null));
        window.location.reload();
    };

    const renderIcon = () => {
        return (
            <div
                id="basic-button"
                aria-controls={open ? 'basic-menu' : undefined}
                aria-haspopup="true"
                aria-expanded={open ? 'true' : undefined}
                onClick={handleClick}
                className="profile-menu-flex"
            >
                <MenuRoundedIcon />
                <Avatar size="small" icon={<UserOutlined />} src={currentUser?.avatar || ''} />
            </div>
        );
    };

    const renderNavLogin = () => {
        if (currentUser) {
            return (
                <Menu
                    id="basic-menu"
                    anchorEl={anchorEl}
                    open={open}
                    onClose={handleClose}
                    MenuListProps={{
                        'aria-labelledby': 'basic-button',
                    }}
                    sx={{
                        '.MuiPaper-root': {
                            minWidth: '200px',
                            borderRadius: '1rem',
                            boxShadow: '0 1px 2px rgb(0 0 0/8%), 0 4px 12px rgb(0 0 0/5%)',
                        },
                    }}
                >
                    <MenuItem>
                        <NavLink to={'/personalInfo'} className="no-underline">
                            <p className="my-0">Thông tin cá nhân</p>
                        </NavLink>
                    </MenuItem>
                    <hr />
                    <div
                        style={{
                            height: '1px',
                            backgroundColor: 'var(--grey)',
                            width: '100%',
                        }}
                    ></div>
                    <MenuItem onClick={handleClose} className="menu-items">
                        Cho thuê nhà
                    </MenuItem>
                    <MenuItem onClick={handleClose} className="menu-items">
                        Tổ chức trải nghiệm
                    </MenuItem>
                    <MenuItem onClick={handleClose} className="menu-items">
                        Trợ giúp
                    </MenuItem>
                    <hr />
                    <MenuItem onClick={handleLogout} className="menu-items">
                        Đăng xuất
                    </MenuItem>
                </Menu>
            );
        } else {
            return (
                <Menu
                    id="basic-menu"
                    anchorEl={anchorEl}
                    open={open}
                    onClose={handleClose}
                    MenuListProps={{
                        'aria-labelledby': 'basic-button',
                    }}
                    sx={{
                        '.MuiPaper-root': {
                            minWidth: '200px',
                            borderRadius: '1rem',
                            boxShadow: '0 1px 2px rgb(0 0 0/8%), 0 4px 12px rgb(0 0 0/5%)',
                        },
                    }}
                >
                    <MenuItem>
                        <NavLink to={'/register'} className="no-underline">
                            <p className="my-0">Đăng ký ngay</p>
                        </NavLink>
                    </MenuItem>
                    <MenuItem>
                        <NavLink to={'/login'} className="no-underline mt-2">
                            <p className="my-0">Đăng nhập ngay</p>
                        </NavLink>
                    </MenuItem>
                    <hr />
                    <MenuItem onClick={handleClose} className="menu-items">
                        Trợ giúp
                    </MenuItem>
                </Menu>
            );
        }
    };

    return (
        <div className="shadow-2xl">
            {renderIcon()}
            {renderNavLogin()}
        </div>
    );
}
