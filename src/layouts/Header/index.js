import './style.css';
import BasicMenu from './ProfileMenu';
import React, { useState } from 'react';
import 'react-date-range/dist/styles.css';
import logo from '~/assets/image/logo.png';
import SliderPage from '../Slider/SliderPage';
import { Link, NavLink } from 'react-router-dom';
import 'react-date-range/dist/theme/default.css';
import SimpleBottomNavigation from './BottomNav';
import GroupIcon from '@mui/icons-material/Group';
import { SearchOutlined } from '@ant-design/icons';
import { DateRangePicker } from 'react-date-range';
import LanguageIcon from '@mui/icons-material/Language';
import MobileSearchBar from '~/components/MobileSearchBar/MobileSearchBar';

function Header() {
    const [searchInput, setSearchInput] = useState('');
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());
    const [noOfGuests, setNoOfGuests] = useState(1);

    const handleSelect = (ranges) => {
        setStartDate(ranges.selection.startDate);
        setEndDate(ranges.selection.endDate);
    };

    const resetInput = () => {
        setSearchInput('');
    };

    const selectionRange = {
        startDate: startDate,
        endDate: endDate,
        key: 'selection',
    };

    return (
        <div className="container mx-auto">
            <div className="navbar">
                <NavLink to={'/'} className="flex align-items-center">
                    <img src={logo} alt="logo" className="navbar-logo" />
                    <p className="text-red-500 text-2xl justify-center mb-0 pt-1 px-2 font-black">airbnb</p>
                </NavLink>

                <div className="search-bar py-3">
                    <div className="search-bar-text">Địa điểm bất kỳ</div>
                    <div className="search-bar-text">Tuần bất kỳ</div>
                    <div className="search-bar-text">Thêm khách</div>
                </div>
                <div className="flex items-center md:border-2 rounded-full py-2 md:shadow-sm">
                    <input
                        value={searchInput}
                        onChange={(e) => setSearchInput(e.target.value)}
                        className="flex-grow pl-5 bg-transparent outline-none text-sm text-gray-600 placeholder-gray-400"
                        type="text"
                        placeholder="Search..."
                    />
                    <SearchOutlined className="hidden md:inline-flex h-8 bg-red-400 text-white rounded-full p-2 cursor-pointer md:mx-2" />
                </div>
                <div className="profile-container">
                    <div className="airbnb-your-home">Trở thành chủ nhà</div>
                    <div className="airbnb-your-home">
                        <LanguageIcon sx={{ fontSize: '1.3rem' }} />
                    </div>
                    <div className="profile-div hover:cursor-pointer border-2 rounded-2xl">
                        <BasicMenu />
                    </div>
                </div>

                <MobileSearchBar />
                <SimpleBottomNavigation />

                {searchInput && (
                    <div className="flex flex-col col-span-3 mx-auto">
                        <DateRangePicker
                            ranges={[selectionRange]}
                            minDate={new Date()}
                            rangeColors={['#FD5B61']}
                            onChange={handleSelect}
                        />
                        <div className="flex items-center border-b mb-4">
                            <h2 className="text-2xl flex-grow font-semibold">Number of Guests</h2>
                            <GroupIcon className="h-5" />
                            <input
                                value={noOfGuests}
                                onChange={(e) => setNoOfGuests(e.target.value)}
                                type="number"
                                min={1}
                                className="w-12 pl-2 text-lg outline-none text-red-400"
                            />
                        </div>
                        <div className="flex">
                            <button onClick={resetInput} className="flex-grow text-gray-500">
                                Cancel
                            </button>

                            <Link className="flex-grow text-gray-400 no-underline" to={`/search`}>
                                Search
                            </Link>
                        </div>
                    </div>
                )}
            </div>

            <SliderPage />
        </div>
    );
}

export default Header;
