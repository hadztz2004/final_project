import { Link } from 'react-router-dom';

const footers = [
    {
        title: 'Hỗ trợ',
        sub: [
            {
                item: 'Trung tâm trợ giúp',
                path: '/',
            },
            {
                item: 'AirCover',
                path: '/',
            },
            {
                item: 'Thông tim an toàn',
                path: '/',
            },
            {
                item: 'Hỗ trợ người khuyết tật',
                path: '/',
            },
            {
                item: 'Các tùy chọn hủy',
                path: '/',
            },
            {
                item: 'Biện pháp ứng phó với dịch COVID-19',
                path: '/',
            },
            {
                item: 'Báo cáo lo ngại của hàng xóm',
                path: '/',
            },
        ],
    },
    {
        title: 'Cộng đồng',
        sub: [
            {
                item: 'Airbnb.org: nhà ở cứu trợ',
                path: '/',
            },
            {
                item: 'Hỗ trợ dân tị nạn Afghanistan',
                path: '/',
            },
            {
                item: 'Chống phân biệt đối xử',
                path: '/',
            },
        ],
    },
    {
        title: 'Đón tiếp khách',
        sub: [
            {
                item: 'Thử đón tiếp khách',
                path: '/',
            },
            {
                item: 'AirCover cho Chủ nhà',
                path: '/',
            },
            {
                item: 'Xem tài nguyên đón tiếp khách',
                path: '/',
            },
            {
                item: 'Truy cập diễn đàn cộng đồng',
                path: '/',
            },
            {
                item: 'Đón tiếp khách có trách nhiệm',
                path: '/',
            },
        ],
    },
    {
        title: 'Airbnb',
        sub: [
            {
                item: 'Trang tin tức',
                path: '/',
            },
            {
                item: 'Tìm hiểu các tính năng mới',
                path: '/',
            },
            {
                item: 'Thư ngỏ từ các nhà sáng lập',
                path: '/',
            },
            {
                item: 'Cơ hội nghề nghiệp',
                path: '/',
            },
            {
                item: 'Nhà đầu tư',
                path: '/',
            },
        ],
    },
];

function Footer() {
    return (
        <>
            <div className="border-t mt-5 pt-5 bg-gray-100">
                <footer className="container grid grid-cols-2 mx-auto gap-x-3 gap-y-8 sm:grid-cols-3 md:grid-cols-4 px-10 pb-10 mb-5">
                    {footers.map((footer, index) => (
                        <div className="flex flex-col space-y-4" key={index}>
                            <h3 className="font-semibold text-lg">{footer.title}</h3>
                            <div className="flex flex-col space-y-2 text-sm dark:text-gray-400">
                                {footer.sub.map((item, index) => (
                                    <Link className="nav-link hover:underline" to={item.path} key={index}>
                                        {item.item}
                                    </Link>
                                ))}
                            </div>
                        </div>
                    ))}
                </footer>
            </div>
            <div className="bg-gray-50 border-t fixed bottom-0 w-screen z-10  py-3 hidden md:block">
                <div className="container mx-auto px-10 flex justify-between items-center text-gray-500 ">
                    <div>
                        <span>© 2022 Airbnb, Inc.</span>
                        <span className="px-3 hover:underline cursor-pointer">Quyền riêng tư</span>.
                        <span className="px-3 hover:underline cursor-pointer">Điều khoản</span>.
                        <span className="px-3 hover:underline cursor-pointer">Sơ đồ trang web</span>.
                    </div>
                    <div className="text-gray-700">
                        <i className="fa-solid fa-earth-oceania"></i>
                        <span className="hover:underline cursor-pointer px-3 font-medium">Tiếng Việt(VN)</span>
                        <i className="fa fa-dollar-sign font-medium cursor-pointer"></i>
                        <span className="hover:underline cursor-pointer px-2 font-medium">USD</span>
                        <span className="font-medium hover:underline">
                            Hỗ trợ tài nguyên <i className="fa fa-angle-up"></i>
                        </span>
                    </div>
                </div>
            </div>
        </>
    );
}

export default Footer;
