import { Link } from 'react-router-dom';
import { StarFilled } from '@ant-design/icons';

function RoomList({ roomArr }) {
    const renderRoomList = () =>
        roomArr.slice(0, 15).map((item, index) => (
            <Link to={`/roomdetail/${item.id}`} style={{ color: 'black' }} key={index}>
                <div className="relative">
                    <div className="grad absolute w-full h-full rounded-b-[1.3rem]"></div>
                    <div className="flex ">
                        <img
                            src={item.hinhAnh}
                            alt=""
                            className="object-cover rounded-[1.3rem] sm:h-[rem]  md:h-[20rem] w-full"
                        />
                    </div>
                </div>

                <div className="pt-3 flex justify-between items-start">
                    <div>
                        <p className="max-w-[17rem] font-semibold overflow-hidden">{item.tenPhong}</p>
                        <p className="max-w-[17rem]  text-[16px] -mt-1 text-gray-500">Jan 28 - Aug 9</p>
                        <p className="max-w-[17rem] font-semibold text-[17px]">${item.giaTien}</p>
                    </div>

                    <div className="flex items-center space-x-1">
                        <StarFilled />
                        <p className="text-[15px]">5.0</p>
                    </div>
                </div>
            </Link>
        ));

    return <div className="grid grid-cols-3 gap-4">{renderRoomList()}</div>;
}

export default RoomList;
