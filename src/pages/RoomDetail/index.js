import dayjs from 'dayjs';
import Renderrate from './Renderrate';
import { useSelector } from 'react-redux';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { getRoomById } from '~/services/roomService';
import { postUserBooking } from '~/services/bookingService';
import { DatePicker, message, Space, Button, Popconfirm } from 'antd';

const arrWhat = [
    {
        icon: 'utensils',
        title: 'Bếp',
    },
    {
        icon: 'wifi',
        title: 'Wi-fi',
    },
    {
        icon: 'tv',
        title: 'TV',
    },
    {
        icon: 'water',
        title: 'Máy sấy khô',
    },
    {
        icon: 'temperature-quarter',
        title: 'Hệ thống sưởi',
    },
    {
        icon: 'umbrella-beach',
        title: 'Hồ bơi',
    },
];

const reviews = [
    {
        title: 'Mức độ sạch sẽ',
        style: '100%',
        star: '5,0',
    },
    {
        title: 'Độ chính xác',
        style: '100%',
        star: '5,0',
    },
    {
        title: 'Giao tiếp',
        style: '100%',
        star: '5,0',
    },
    {
        title: 'Vị trí',
        style: '95%',
        star: '4,9',
    },
    {
        title: 'Nhận phòng',
        style: '100%',
        star: '5,0',
    },
    {
        title: 'Giá trị',
        style: '100%',
        star: '5,0',
    },
];

const { RangePicker } = DatePicker;

function RoomDetail() {
    const [dateBooking, setDateBooking] = useState([0, 0]);
    const [room, setRoom] = useState(null);
    const [client, setClient] = useState(0);
    const { id } = useParams();
    const currentUser = useSelector((state) => state.auth.currentUser);

    const time = (day1, day2) => {
        if (day1 && day2) {
            const get_day_of_time = (d1, d2) => {
                let ms1 = d1.getTime();
                let ms2 = d2.getTime();
                return Math.ceil((ms2 - ms1) / (24 * 60 * 60 * 1000));
            };

            let date1 = new Date(day1);
            let date2 = new Date(day2);
            let time = get_day_of_time(date1, date2);

            return time;
        } else {
            let time = 0;
            return time;
        }
    };

    const disabledDate = (current) => {
        return current && current < dayjs().endOf('day');
    };
    function onChange(e) {
        let date = e?.map((item) => {
            return dayjs(item).format('YYYY-MM-DD');
        });
        if (date === undefined) {
            date = [0, 0];
        }
        setDateBooking(date);
    }

    useEffect(() => {
        const fetchApi = async () => {
            const result = await getRoomById(id);

            setRoom(result.content);
        };

        fetchApi();
    }, [id]);

    const handleBooking = async () => {
        const userBooking = {
            id: currentUser.id,
            maPhong: room.id,
            ngayDen: dateBooking[0],
            ngayDi: dateBooking[1],
            soLuongKhach: client,
            maNguoiDung: currentUser.id,
        };
        const result = await postUserBooking(userBooking);
        if (result.statusCode === 201) {
            message.success('Booking Success');
        } else {
            message.error('Booking Fails');
        }
    };

    return (
        <div className="px-20 pt-6">
            <button className="mr-3 inline-block text-3xl">
                <i className="fa-solid fa-language"></i>
            </button>
            <span className="font-semibold text-xl sm:text-3xl tracking-widest leading-relaxed text-gray-900">
                {room?.tenPhong}
            </span>
            <div className="flex flex-wrap justify-between items-center">
                <div>
                    <span className="text-sm font-normal tracking-widest">
                        <i className="fa-solid fa-star"></i>4 .
                    </span>
                    <span className="underline text-sm font-normal tracking-widest mx-1">51 đánh giá</span>.
                    <span className="text-sm font-normal tracking-widest mx-1">
                        <i className="fa-solid fa-award"></i> Chủ nhà siêu cấp .
                    </span>
                    <span className="underline text-sm font-normal tracking-widest mx-1">
                        Khu du lịch sinh thái Hồng Hào, Bến tre, viet nam
                    </span>
                </div>
                <div className="flex flex-wrap justify-center items-center">
                    <button className="px-2 py-1 hover:bg-gray-100 rounded-md transition-all duration-150 flex justify-center items-center font-semibold text-sm text-gray-700">
                        <i className="fa-solid fa-arrow-up-from-bracket"></i>
                        <span className="ml-2">Chia sẻ</span>
                    </button>
                    <button className="px-2 py-1 hover:bg-gray-100 rounded-md transition-all duration-150  flex justify-center items-center font-semibold text-sm text-gray-700">
                        <i className="fa-regular fa-heart"></i>
                        <span className="ml-2">Lưu</span>
                    </button>
                </div>
            </div>

            <div className="w-100 mt-5">
                <img
                    className="w-full object-contain rounded-xl overflow-hidden w-100"
                    alt=""
                    src={room?.hinhAnh}
                    style={{ imageRendering: 'pixelated' }}
                />
            </div>

            <div className="w-full flex sm:flex-row flex-col mt-10 border-b pb-5">
                <div className="w-full sm:w-1/2 lg:w-3/5">
                    <div className="flex flex-wrap justify-between items-center pb-5 border-b">
                        <div>
                            <h1 className="font-semibold text-lg sm:text-2xl text-gray-800">
                                Toàn bộ căn hộ. Chủ nhà Sungwon
                            </h1>
                            <span className="text-sm font-normal  tracking-widest text-gray-700">
                                <span>2 khách . </span>
                                <span className=" mx-1">2 phòng ngủ . </span>
                                <span className=" mx-1">3 phòng tắm</span>
                            </span>
                        </div>
                        <div className="w-12 h-12">
                            <img
                                className="w-full h-full rounded-full overflow-hidden"
                                alt=""
                                src="https://cdn3.ivivu.com/2018/05/ngoi-chua-co-tuong-phat-bang-toc-nguoi-lon-nhat-viet-nam-ivivu-1.jpg"
                            />
                        </div>
                    </div>
                    <div className="mt-5 pb-5 border-b">
                        <div className="flex w-full">
                            <div className="pt-2">
                                <i className="fa-solid fa-medal"></i>
                            </div>
                            <div className="ml-4 w-full overflow-hidden">
                                <h3 className="font-semibold text-gray-800 text-base sm:text-lg ">
                                    Sungwon là Chủ nhà siêu cấp
                                </h3>
                                <p className="tracking-wider text-gray-500 block">
                                    Chủ nhà siêu cấp là những chủ nhà có kinh nghiệm, được đánh giá cao và là những
                                    người cam kết mang lại quãng thời gian ở tuyệt vời cho khách.
                                </p>
                            </div>
                        </div>
                        <div className="flex mt-5">
                            <div className="pt-2">
                                <i className="fa-solid fa-location-dot"></i>
                            </div>
                            <div className="ml-4">
                                <h3 className="font-semibold text-gray-800 text-base sm:text-lg ">
                                    Địa điểm tuyệt vời
                                </h3>
                                <p className="tracking-wider text-gray-500">
                                    90% khách gần đây đã xếp hạng 5 sao cho vị trí này.
                                </p>
                            </div>
                        </div>
                        <div className="flex mt-5">
                            <div className="pt-2">
                                <i className="fa-regular fa-calendar"></i>
                            </div>
                            <div className="ml-4">
                                <h3 className="font-semibold text-gray-800 text-base sm:text-lg ">
                                    Miễn phí hủy trong 48 giờ.
                                </h3>
                            </div>
                        </div>
                    </div>

                    <div className="mt-5 pb-5 border-b overflow-hidden">
                        <img
                            src="https://a0.muscache.com/im/pictures/54e427bb-9cb7-4a81-94cf-78f19156faad.jpg"
                            alt=""
                            className="h-7 mb-4"
                        />
                        <p className="text-base tracking-wider text-gray-800 mb-2">
                            Mọi đặt phòng đều được bảo vệ miễn phí trong trường hợp Chủ nhà hủy, thông tin nhà/phòng cho
                            thuê không chính xác và những vấn đề khác như sự cố trong quá trình nhận phòng.
                        </p>
                        <button className="font-semibold underline text-base text-gray-800  tracking-wider">
                            Tìm hiểu thêm
                        </button>
                    </div>

                    <div className="mt-5 pb-5 border-b overflow-hidden">
                        <div className="flex mb-4">
                            <div className="mr-2">
                                <i className="fa-solid fa-language"></i>
                            </div>
                            <div>
                                <span className="text-base tracking-wider text-gray-800">
                                    Một số thông tin đã được dịch tự động.
                                </span>
                                <button className="underline font-semibold text-base tracking-wider text-gray-800 inline-block">
                                    Hiển thị ngôn ngữ gốc
                                </button>
                            </div>
                        </div>
                        <p className="text-base tracking-wider text-gray-800 mb-4">
                            Nhà nghỉ thôn dã hình lưỡi liềm trong một ngôi làng nghệ thuật gốm hai nghìn năm. Một ngôi
                            nhà nguyên khối lớn với sân thượng ba tầng của Bảo tàng Văn hóa Guitar Serra, nổi tiếng với
                            mặt tiền đặc sắc trong một ngôi làng nghệ thuật gốm hai nghìn năm pha trộn rất tốt với thiên
                            nhiên.
                        </p>
                        <p className="text-base tracking-wider text-gray-800 mb-4">
                            Tận hưởng kỳ nghỉ dưỡng sức cảm xúc thư giãn trong một căn phòng ấm cúng, chào...
                        </p>
                        <button className="underline font-semibold text-base tracking-wider text-gray-800">
                            Hiển thị thêm
                            <span className="ml-1">
                                <i className="fa-solid fa-chevron-right"></i>
                            </span>
                        </button>
                    </div>

                    <div className="mt-5 pb-5">
                        <h2 className="font-semibold text-gray-800 text-xl pb-4">Nơi này có những gì cho bạn</h2>

                        <div className="grid grid-cols-2">
                            {arrWhat.map((item, index) => (
                                <div className="flex items-center pb-4" key={index}>
                                    <div>
                                        <i className={`fa-solid fa-${item.icon}`}></i>
                                    </div>
                                    <div className="ml-4 text-base tracking-wider text-gray-800">{item.title}</div>
                                </div>
                            ))}
                        </div>
                        <div className="mt-5">
                            <button className="border border-solid border-gray-900 hover:bg-gray-100 transition-all duration-200 rounded-md px-5 py-3 font-semibold text-base text-gray-800 tracking-wider">
                                Hiển thị tất cả 75 tiện nghi
                            </button>
                        </div>
                    </div>
                </div>
                <div className="w-full sm:w-1/2 lg:w-2/5">
                    <div className="sticky top-28">
                        <div className="bg-white shadow-xl border rounded-xl p-6 w-full lg:w-5/6 mx-auto">
                            <div className="relative w-full">
                                <div className="hidden md:flex justify-between items-center mb-4">
                                    <div>
                                        <span>$ </span>
                                        <span className="text-xl font-semibold">{room?.giaTien}</span>
                                        <span className="text-base"> đêm</span>
                                    </div>
                                    <div>
                                        <span className="text-sm font-normal">
                                            <i className="fa fa-star"></i> 4 .
                                        </span>
                                        <span className="underline text-sm font-normal tracking-widest mx-1">
                                            {client * 3} đánh giá
                                        </span>
                                    </div>
                                </div>

                                <div className="flex flex-col border border-solid border-gray-400 rounded-md">
                                    <div className="flex w-full border-b border-solid border-gray-400">
                                        <Space direction="vertical">
                                            <Space className="flex justify-around">
                                                <div className="text-xl uppercase font-semibold text-red-400">
                                                    Nhận phòng
                                                </div>
                                                <div className="text-xl uppercase font-semibold text-red-400">
                                                    Trả phòng
                                                </div>
                                            </Space>
                                            <RangePicker
                                                disabledDate={disabledDate}
                                                className="border-none ml-5"
                                                onChange={onChange}
                                            />
                                        </Space>
                                    </div>
                                    <div className="p-2">
                                        <div className="uppercase text-xs font-semibold">Khách</div>
                                        <div className="flex justify-between items-center m-1">
                                            <button
                                                className="w-8 h-8 bg-gray-300 hover:bg-red-400 duration-200 rounded-xl text-white cursor-pointer"
                                                onClick={() => client > 0 && setClient(client - 1)}
                                            >
                                                -
                                            </button>
                                            <div>{client} khách</div>
                                            <button
                                                className="w-8 h-8 bg-gray-300 hover:bg-red-400 duration-200 rounded-xl text-white cursor-pointer"
                                                onClick={() => {
                                                    client < 5 && setClient(client + 1);
                                                }}
                                            >
                                                +
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <Popconfirm
                                    className="w-full text-3xl  font-semibold h-full "
                                    title={<h2>Bạn muốn đặt phòng</h2>}
                                    onConfirm={handleBooking}
                                    icon={false}
                                    okType="danger"
                                >
                                    <Button
                                        className="w-full py-3  mt-3 rounded-lg text-white text-lg font-semibold"
                                        style={{
                                            background:
                                                'linear-gradient(to right, rgb(230, 30, 77) 0%, rgb(227, 28, 95) 50%, rgb(215, 4, 102) 100%)',
                                        }}
                                    >
                                        Đặt phòng
                                    </Button>
                                </Popconfirm>

                                <div className="text-center font-normal text-gray-400 my-2">
                                    <span>Bạn vẫn chưa bị trừ tiền</span>
                                </div>

                                <div className="border-b py-2">
                                    <div className="flex justify-between py-1 text-base">
                                        <div className="underline text-gray-600">
                                            $ {room?.giaTien} x {time(dateBooking[0], dateBooking[1])} đêm
                                        </div>
                                        <div>
                                            <span>{room?.giaTien * time(dateBooking[0], dateBooking[1])}</span> $
                                        </div>
                                    </div>
                                    <div className="flex justify-between py-1 text-base">
                                        <div className="underline text-gray-600">Phí dịch vụ</div>
                                        <div>
                                            <span>0</span> $
                                        </div>
                                    </div>
                                </div>

                                <div className="flex justify-between items-center text-lg font-semibold pt-3">
                                    <div>Tổng trước thuế</div>
                                    <div>{room?.giaTien * time(dateBooking[0], dateBooking[1])} $</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="mt-10 pb-5 border-b">
                <div>
                    <h2 className="font-semibold text-gray-800 text-xl pb-4 flex items-center">
                        <div className="flex items-center">
                            <i className="fa-solid fa-star"></i>
                        </div>
                        <div className="ml-2">1 . </div>
                        <div className="ml-2">62 đánh giá</div>
                    </h2>
                </div>

                <div className="grid grid-cols-1 sm:grid-cols-2 gap-x-20 gap-y-4">
                    {reviews.map((item, index) => (
                        <div className="flex justify-between items-center" key={index}>
                            <div className="w-full text-base tracking-wide text-gray-700">{item.title}</div>
                            <div className="w-1/2 flex justify-between items-center">
                                <div className="w-full bg-gray-200 rounded-full h-1">
                                    <div
                                        className="bg-gray-800 h-1 rounded-full"
                                        style={{ width: `${item.style}` }}
                                    ></div>
                                </div>
                                <div className="ml-4">{item.star}</div>
                            </div>
                        </div>
                    ))}
                </div>
                <div className="grid grid-cols-1 sm:grid-cols-2 sm:gap-x-20 gap-y-4 sm:w-4/5 mt-5">
                    <div className="sm:col-span-2">
                        <button className="border border-solid border-gray-900 hover:bg-gray-100 transition-all duration-200 rounded-md px-5 py-3 font-semibold text-base text-gray-800 tracking-wider">
                            Hiển thị tất cả 120 đánh giá
                        </button>
                    </div>
                    <Renderrate id={id} />
                </div>
            </div>
            <div className="mt-10 pb-5 border-b"></div>
        </div>
    );
}

export default RoomDetail;
