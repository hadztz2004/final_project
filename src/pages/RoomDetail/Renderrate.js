import { Avatar, Descriptions } from 'antd';
import React, { useEffect, useState } from 'react';
import { getComment } from '~/services/commentService';
import { LikeTwoTone, UserOutlined } from '@ant-design/icons';

function Renderrate({ id }) {
    const [comment, setComment] = useState([]);

    useEffect(() => {
        const fetchApi = async () => {
            const result = await getComment(id);

            setComment(result.content);
        };

        fetchApi();
    }, [id]);

    const renderRate = () => {
        return comment.map((item, index) => {
            return (
                <div key={index} className="bg-slate-100 m-2 rounded-xl relative">
                    <Avatar src={item.avatar} icon={<UserOutlined />} />
                    <Descriptions title={item.tenNguoiBinhLuan} size="small">
                        <Descriptions.Item label={item.ngayBinhLuan}>{item.noiDung}</Descriptions.Item>
                    </Descriptions>
                    <div className="absolute bottom-0 right-0 mr-2 flex flex-row">
                        <LikeTwoTone className="hover:scale-150" />
                        <div>{item.saoBinhLuan}</div>
                    </div>
                </div>
            );
        });
    };
    return <div style={{ height: 400, overflowY: 'scroll' }}> {renderRate()}</div>;
}
export default Renderrate;
