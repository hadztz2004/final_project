import RoomList from '../Room/RoomList';
import { useEffect, useState } from 'react';
import { getRoomList } from '~/services/roomService';

function Home() {
    const [roomArr, setRoomArr] = useState([]);

    useEffect(() => {
        const fetchApi = async () => {
            const result = await getRoomList();
            setRoomArr(result.content);
        };
        fetchApi();
    }, []);

    return (
        <div>
            <div className="container mx-auto">
                <RoomList roomArr={roomArr} />
            </div>
        </div>
    );
}

export default Home;
