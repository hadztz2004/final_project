import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { updateUser } from '~/services/user';
import { Modal, Button, Form } from 'react-bootstrap';
import { loginUserSuccess } from '~/redux/reducer/authReducer';

function PopupUser({ user, show, setShow }) {
    const [email, setEmail] = useState(user?.email);
    const [gender, setGender] = useState(user?.gender);
    const [name, setName] = useState(user?.name);
    const [phone, setPhone] = useState(user?.phone);
    const dispatch = useDispatch();

    const handleChanges = async () => {
        const regexEmail = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
        if (!regexEmail.test(email) || !email) {
            alert('Email invalidate');
            return;
        }
        if (!name) {
            alert('Name is required');
            return;
        }

        const userUpdate = {
            name,
            email,
            phone: phone || '',
            birthday: user.birthday,
            gender,
        };

        const result = await updateUser(userUpdate, user.id);
        if (result.statusCode === 200) {
            dispatch(loginUserSuccess(result.content));
            window.location.reload();
        } else {
            alert('User update error');
        }
    };

    return (
        <Modal show={show} onHide={() => setShow(false)}>
            <Modal.Header closeButton>
                <Modal.Title>Sửa thông tin người dùng</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form.Group className="mb-3">
                    <Form.Label>Email</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter your email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                    />
                </Form.Group>
                <Form.Group className="mb-3">
                    <Form.Label>Gender</Form.Label>
                    <select className="form-control" value={gender} onChange={(e) => setGender(e.target.value)}>
                        <option value={true}>Male</option>
                        <option value={false}>Female</option>
                    </select>
                </Form.Group>
                <Form.Group className="mb-3">
                    <Form.Label>Name</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter your name"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                    />
                </Form.Group>
                <Form.Group className="mb-3">
                    <Form.Label>Phone</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter your phone"
                        value={phone}
                        onChange={(e) => setPhone(e.target.value)}
                    />
                </Form.Group>
            </Modal.Body>
            <Modal.Footer>
                <Button size="sm" variant="secondary" onClick={() => setShow(false)}>
                    Close
                </Button>
                <Button size="sm" variant="primary" onClick={handleChanges}>
                    Save Changes
                </Button>
            </Modal.Footer>
        </Modal>
    );
}

export default PopupUser;
