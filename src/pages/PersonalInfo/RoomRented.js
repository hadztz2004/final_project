import { Popover, Rate, Tag } from 'antd';
import React, { useEffect, useState } from 'react';
import { getRoomById } from '~/services/roomService';
import { CheckOutlined, CoffeeOutlined, SkinOutlined } from '@ant-design/icons';

function RoomRented({ roomId }) {
    const [arrData, setArrData] = useState([]);

    const renderListRoomBooking = () =>
        arrData.map((item, index) => (
            <div key={index} className="py-1">
                <div className="bg-gray-100 rounded-xl" style={{ width: '90%' }}>
                    <div className="flex">
                        <div style={{ width: '40%' }} className="object-cover">
                            <img src={item.hinhAnh} alt="" className="rounded-xl h-full" />
                        </div>
                        <div className="whitespace-normal px-2 relative m-2" style={{ width: '59%' }}>
                            <span className="text-lg">{item.tenPhong}</span> <br />
                            <span className="text-xs text-gray-600">
                                2 Phòng khách - 1 Phòng Studio - 1 Giường - 1 Phòng tắm
                                <br />
                                Wifi - Bếp - Điều hòa - Máy giặt
                            </span>
                            <div className="pr-56">
                                <hr />
                            </div>
                            <Popover content={item.moTa}>
                                <span>
                                    {item.moTa.slice(0, 20)}... <i className="text-xs">xem thêm</i>
                                </span>
                            </Popover>
                            <div>
                                <Tag color="green">
                                    <CheckOutlined />
                                    <SkinOutlined />
                                    <CoffeeOutlined />
                                </Tag>
                            </div>
                            <div className="absolute bottom-0 right-0 ">
                                chỉ với <b>{item.giaTien * 30}$</b>/tháng
                            </div>
                            <div className="absolute top-0 right-0 ">
                                <Rate defaultValue={Math.floor(Math.random() * 6)} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ));

    useEffect(() => {
        let result = [];

        roomId.map(async (item) => {
            const res = await getRoomById(item);
            let roomDetail = {
                giaTien: res.content.giaTien,
                hinhAnh: res.content.hinhAnh,
                moTa: res.content.moTa,
                tenPhong: res.content.tenPhong,
            };
            result.push(roomDetail);
            setArrData(result);
        });

        renderListRoomBooking();
    }, [roomId]);

    return <div style={{ height: 400, overflowY: 'auto' }}>{renderListRoomBooking()}</div>;
}
export default RoomRented;
