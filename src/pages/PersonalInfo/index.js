import { Avatar, Card } from 'antd';
import PopupUser from './PopupUser';
import RoomRented from './RoomRented';
import { useSelector } from 'react-redux';
import PopupAvartar from './PopupAvartar';
import React, { useEffect, useState } from 'react';
import { getBookingByUserCode } from '~/services/bookingService';

const { Meta } = Card;

export default function PersonalInfo() {
    const [show, setShow] = useState(false);
    const [userEdit, setUserEdit] = useState(null);
    const [showAvartar, setShowAvartar] = useState(false);
    const [avatarUserEdit, setAvatarUserEdit] = useState(null);
    const [roomId, setRoomId] = useState([]);
    const currentUser = useSelector((state) => state.auth.currentUser);

    useEffect(() => {
        const fetchApi = async () => {
            const result = await getBookingByUserCode(currentUser.id);

            if (result.statusCode === 200) {
                const id = result.content.map((item) => item.maPhong);
                setRoomId(id);
            }
        };
        fetchApi();
    }, []);

    const handleShowModal = (user) => {
        setShow(true);
        setUserEdit(user);
    };

    const handleShowAvartar = (user) => {
        setShowAvartar(true);
        setAvatarUserEdit(user);
    };

    return (
        <div className="flex bg-white py-5">
            <Card
                hoverable
                style={{
                    width: 350,
                    height: '70vh',
                }}
            >
                <div className="m-auto w-1/2 my-4">
                    <Avatar src={currentUser.avatar} size={120} className="ml-3" />

                    <p
                        className="hover:underline hover:text-blue-700 text-center text-gray-600"
                        onClick={() => {
                            handleShowAvartar(currentUser);
                        }}
                    >
                        cập nhật ảnh
                    </p>
                </div>
                <Meta
                    avatar={<Avatar src="https://tse1.mm.bing.net/th?id=OIP.NbuGY8vYnkDy2o7xSBO3LAHaHa&pid=Api&P=0" />}
                    title="Xác minh danh tính"
                />

                <p>Xác thực danh tính của bạn với huy hiệu xác minh</p>
                <button className="bg-gray-200 text-gray-700 font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-gray-300 hover:shadow-lg focus:bg-gray-300 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-gray-400 active:shadow-lg transition duration-150 ease-in-out px-4 py-2 my-2">
                    Nhận huy hiệu
                </button>
                <hr />
                <i>
                    <Meta title="Đã xác nhận" description="✔ Địa chỉ email" />
                </i>
            </Card>
            <div className="ml-10">
                <h1>Xin chào tôi là {currentUser.name}</h1>
                <p>Bắt đầu tham gia vào năm 2023</p>
                <p
                    className="text-blue-400 hover:text-blue-700 hover:underline"
                    onClick={() => handleShowModal(currentUser)}
                >
                    Chỉnh sửa thông tin
                </p>

                {show && <PopupUser user={userEdit} show={show} setShow={setShow} />}
                <PopupAvartar showAvartar={showAvartar} setShowAvartar={setShowAvartar} user={avatarUserEdit} />
                <h2 className="text-3xl">Phòng đã thuê</h2>

                <RoomRented roomId={roomId} />
            </div>
        </div>
    );
}
