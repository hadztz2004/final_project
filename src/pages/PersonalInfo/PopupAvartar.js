import React, { useState } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { loginUserSuccess } from '~/redux/reducer/authReducer';
import { updateAvatar } from '~/services/user';

function PopupAvartar({ showAvartar, setShowAvartar, user }) {
    const [avatar, setAvatar] = useState(user?.avatar);

    const dispatch = useDispatch();
    const currentUser = useSelector((state) => state.auth.currentUser);

    const handleChange = (e) => {
        const file = e.target.files[0];
        setAvatar(file);
    };

    const handleSave = async (avatar) => {
        const formFile = new FormData();
        formFile.append('formFile', avatar);

        const result = await updateAvatar(formFile, currentUser.token);

        if (result.statusCode === 200) {
            dispatch(loginUserSuccess({ ...result.content, token: currentUser.token }));
        } else {
            alert(result.content);
        }
    };
    return (
        <Modal show={showAvartar} onHide={() => setShowAvartar(false)}>
            <Modal.Header closeButton>
                <Modal.Title>Cập nhật ảnh đại diện</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form.Group className="mb-3">
                    <Form.Control type="file" name="file" onChange={handleChange} />
                </Form.Group>
            </Modal.Body>
            <Modal.Footer>
                <Button size="sm" variant="secondary" onClick={() => setShowAvartar(false)}>
                    Close
                </Button>
                <Button
                    size="sm"
                    variant="primary"
                    onClick={() => {
                        handleSave(avatar);
                    }}
                >
                    Save
                </Button>
            </Modal.Footer>
        </Modal>
    );
}
export default PopupAvartar;
