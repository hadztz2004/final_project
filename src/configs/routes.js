const routes = {
    home: '/',
    login: '/login',
    register: '/register',
    roomdetail: '/roomdetail/:id',
    admin: '/admin',
    personalInfo: '/personalInfo',
    location: '/admin/location',
    bookRoom: '/admin/book-room',
    userId: '/admin/users/:id',
    rooms: '/admin/rooms',
};

export default routes;
