import config from '~/configs';
import Home from '~/pages/Home';
import Login from '~/layouts/Login';
import Admin from '~/components/Admin';
import Register from '~/layouts/Register';
import RoomDetail from '~/pages/RoomDetail';
import Rooms from '~/components/Admin/Rooms';
import PersonalInfo from '~/pages/PersonalInfo';
import Location from '~/components/Admin/Location';
import BookRoom from '~/components/Admin/BookRoom';
import UserDetail from '~/components/Admin/UserDetail';

const router = [
    { path: config.routes.home, component: Home },
    { path: config.routes.login, component: Login, layout: null },
    { path: config.routes.register, component: Register, layout: null },
    { path: config.routes.roomdetail, component: RoomDetail },
    { path: config.routes.admin, component: Admin, layout: null },
    { path: config.routes.personalInfo, component: PersonalInfo },
    { path: config.routes.location, component: Location, layout: null },
    { path: config.routes.bookRoom, component: BookRoom, layout: null },
    { path: config.routes.userId, component: UserDetail, layout: null },
    { path: config.routes.rooms, component: Rooms, layout: null },
];

export default router;
