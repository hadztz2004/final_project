import router from '~/routes';
import { Fragment } from 'react';
import { useSelector } from 'react-redux';
import DefaultLayout from '~/layouts/DefaultLayout';
import ModalForm from './components/Admin/ModalForm';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

function App() {
    const { status } = useSelector((state) => state.modal.modalForm);

    return (
        <Router>
            <div className="App whitespace-nowrap overflow-auto scrollbar-hide">
                {status && <ModalForm />}

                <Routes>
                    {router.map((route, index) => {
                        let Layout = DefaultLayout;

                        if (route.layout) {
                            Layout = route.layout;
                        } else if (route.layout === null) {
                            Layout = Fragment;
                        }

                        const Page = route.component;

                        return (
                            <Route
                                key={index}
                                path={route.path}
                                element={
                                    <Layout>
                                        <Page />
                                    </Layout>
                                }
                            />
                        );
                    })}
                </Routes>
            </div>
        </Router>
    );
}

export default App;
