import request from '~/utils';

export const getUsers = async () => {
    try {
        const res = await request.get('/users');

        return res.data;
    } catch (error) {
        return error.response.data;
    }
};

export const deleteUser = async (id) => {
    try {
        const res = await request.delete('/users', {
            params: {
                id,
            },
        });

        return res.data;
    } catch (error) {
        return error.response.data;
    }
};

export const updateUser = async (user, id) => {
    try {
        const res = await request.put(`/users/${id}`, user);

        return res.data;
    } catch (error) {
        return error.response.data;
    }
};

export const updateAvatar = async (file, token) => {
    try {
        const res = await request.post(`/users/upload-avatar `, file, {
            headers: {
                token,
            },
        });

        return res.data;
    } catch (error) {
        return error.response.data;
    }
};
export const createUser = async (user) => {
    try {
        const res = await request.post('/users', user);

        return res.data;
    } catch (error) {
        return error.response.data;
    }
};
