import request from '~/utils';

export const getRoomList = async () => {
    try {
        const res = await request.get('/phong-thue');

        return res.data;
    } catch (error) {
        return error.response.data;
    }
};

export const getRoomById = async (roomId) => {
    try {
        const res = await request.get(`/phong-thue/${roomId}`);

        return res.data;
    } catch (error) {
        return error.response.data;
    }
};
