import request from '~/utils';

export const getBookingByUserCode = async (userCode) => {
    try {
        const res = await request.get(`/dat-phong/lay-theo-nguoi-dung/${userCode}`);

        return res.data;
    } catch (error) {
        return error.response.data;
    }
};
export const postUserBooking = async (user) => {
    try {
        const res = await request.post(`/dat-phong`, user);

        return res.data;
    } catch (error) {
        return error.response.data;
    }
};
