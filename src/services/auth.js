import request from '~/utils';
import { loginUserSuccess } from '~/redux/reducer/authReducer';

export const loginUser = async (user, dispatch) => {
    try {
        const res = await request.post('/auth/signin', user);

        const { content, ...other } = res.data;
        const userLogin = {
            token: content.token,
            ...content.user,
        };

        dispatch(loginUserSuccess(userLogin));
        return { ...other };
    } catch (error) {
        return error.response.data;
    }
};

export const registerUser = async (user) => {
    try {
        const res = await request.post('/auth/signup', user);

        const { content, ...other } = res.data;

        return { ...other };
    } catch (error) {
        return error.response.data;
    }
};
