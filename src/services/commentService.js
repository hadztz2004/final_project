import request from '~/utils';

export const getComment = async (maPhong) => {
    try {
        const res = await request.get(`/binh-luan/lay-binh-luan-theo-phong/${maPhong}`);

        return res.data;
    } catch (error) {
        return error.response.data;
    }
};
export const postComment = async (user) => {
    try {
        const res = await request.post(`/binh-luan`, user);

        return res.data;
    } catch (error) {
        return error.response.data;
    }
};
