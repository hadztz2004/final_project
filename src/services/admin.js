import request from '~/utils';

export const getLocation = async () => {
    try {
        const res = await request.get('/vi-tri');

        return res.data;
    } catch (error) {
        return error.response.data;
    }
};

export const deleteLocation = async (id, token) => {
    try {
        const res = await request.delete(`/vi-tri/${id}`, {
            headers: {
                token,
            },
        });

        return res.data;
    } catch (error) {
        return error.response.data;
    }
};

export const getBookRoom = async () => {
    try {
        const res = await request.get('/dat-phong');

        return res.data;
    } catch (error) {
        return error.response.data;
    }
};

export const getUserById = async (id) => {
    try {
        const res = await request.get(`/users/${id}`);

        return res.data;
    } catch (error) {
        return error.response.data;
    }
};

export const createLocation = async (location, token) => {
    try {
        const res = await request.post('/vi-tri', location, {
            headers: {
                token,
            },
        });

        return res.data;
    } catch (error) {
        return error.response.data;
    }
};

export const uploadImageLocation = async (id, file, token) => {
    try {
        const res = await request.post('/vi-tri/upload-hinh-vitri', file, {
            headers: {
                token,
            },
            params: {
                maViTri: id,
            },
        });

        return res.data;
    } catch (error) {
        return error.response.data;
    }
};

export const cancelBooking = async (token, id) => {
    try {
        const res = await request.delete(`/phong-thue/${id}`, {
            headers: {
                token,
            },
        });

        return res.data;
    } catch (error) {
        return error.response.data;
    }
};

export const getRooms = async () => {
    try {
        const res = await request.get('/phong-thue');

        return res.data;
    } catch (error) {
        return error.response.data;
    }
};

export const deleteRoom = async (token, id) => {
    try {
        const res = await request.delete(`/phong-thue/${id}`, {
            headers: {
                token,
            },
        });

        return res.data;
    } catch (error) {
        return error.response.data;
    }
};
